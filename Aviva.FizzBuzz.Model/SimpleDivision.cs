﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva.FizzBuzz.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class SimpleDivision : IDivision
    {
        public SimpleDivision() { }

        public SimpleDivision(int nNumber) {
            this.Message = nNumber.ToString();
        }

        /// <summary>
        /// Get and Set Division Number Store
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Get and Set Message color 
        /// </summary>
        public string MessageColor { get; set; }                 
    }

    /// <summary>
    /// FizzDivision (Number Divisiable by 3)
    /// </summary>
    public class FizzDivision : SimpleDivision
    {
        public FizzDivision(){
            Message = DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? "Wizz" : "Fizz";
            MessageColor = "blue";
        }
    }

    /// <summary>
    /// BuzzDivision (Number Divisiable by 5)
    /// </summary>
    public class BuzzDivision : SimpleDivision
    {
        public BuzzDivision() {
            Message = DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? "Wuzz" : "Buzz";
            MessageColor = "green";
        }        
    }

    /// <summary>
    /// FizzBuzzDivision (Number Divisiable by 3 and 5)
    /// </summary>
    public class FizzBuzzDivision : SimpleDivision
    {
        public FizzBuzzDivision() {
            Message = DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? "Wizz Wuzz" : "Fizz Buzz";
        }       
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva.FizzBuzz.Model
{
    /// <summary>
    /// The business logic of the FizzBuzz functionality
    /// </summary>
    public class FizzBuzzLogic : IBusinessLogic
    {

        public FizzBuzzLogic()
        {            
        }         

        /// <summary>
        /// Gets the list of numbers from one upto the number given.
        /// </summary>
        /// <param name="number">The input number.</param>
        /// <returns>List of numbers</returns>
        public List<IDivision> GetList(int number)
        {
            List<IDivision> list = new List<IDivision>();
            for (var counter = 1; counter <= number; counter++){
                list.Add(this.GetDivisionMsg(counter));
            }

            return list;
        }

        /// <summary>
        /// Gets the message corresponding to the number.
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <returns></returns>
        private IDivision GetDivisionMsg(int number){
            //Default set SimpleDivision class instance
            IDivision value = new SimpleDivision(number);
            //Number Divisable by 3 then set FizzDivision class instance
            if (number % 3 == 0){
                value = new FizzDivision();
            }
            //Number Divisable by 5 then set BuzzDivision class instance
            else if (number % 5 == 0){
                value = new BuzzDivision();
            }
            //Number Divisable by 3 and 5 then set FizzBuzzDivision class instance
            if (number % 3 == 0 && number % 5 == 0){
                value = new FizzBuzzDivision();
            }
            return value;
        }
    }
}

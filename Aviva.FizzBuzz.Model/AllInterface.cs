﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva.FizzBuzz.Model
{
    /// <summary>
    /// Abstration of Division Number Classes
    /// </summary>
    public interface IDivision
    {
        /// <summary>
        /// Get and Set Division Number Store
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Get and Set Message color 
        /// </summary>
        string MessageColor { get; set; }      
    }

    /// <summary>
    /// Abstraction of business logic
    /// </summary>
    public interface IBusinessLogic
    {
        /// <summary>
        /// Gets the list of numbers from one upto the number given.
        /// </summary>
        /// <param name="number">The input number.</param>
        /// <returns>List of IDivision</returns>
        List<IDivision> GetList(int nNumber);
    }
}

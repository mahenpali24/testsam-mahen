﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using Aviva.FizzBuzz.Model;
using System.Web.Mvc;
using AvivaTest.Controllers;
using AvivaTest.Models;

namespace AvivaTest.Tests
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        [Test]
        public void Should_give_index_page_for_input_on_request()
        {
            Mock<IBusinessLogic> businessLogic  = new Mock<IBusinessLogic>();
            FizzBuzzController controllerToTest = new FizzBuzzController(businessLogic.Object);
            var resultView                      = controllerToTest.FizzBuzzView() as ViewResult;
            Assert.AreEqual("FizzBuzzView", resultView.ViewName);
        }
        
        [Test]
        public void Should_show_getlist_page_with_20_items_on_one_page()
        {
            Mock<IBusinessLogic> logic = new Mock<IBusinessLogic>();
            logic.Setup(m => m.GetList(21)).Returns(getDivisionList(21));
            FizzBuzzController controller = new FizzBuzzController(logic.Object);
            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 21 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(20, viewModel.DivisionList.Count);
        }

        [Test]
        public void Should_implement_PageCount()
        {
            Mock<IBusinessLogic> logic = new Mock<IBusinessLogic>();
            logic.Setup(m => m.GetList(63)).Returns(getDivisionList(63));
            FizzBuzzController controller = new FizzBuzzController(logic.Object);
            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 63 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(4, viewModel.Page.PageCount);
        }

        [Test]
        public void Should_implement_paging()
        {
            Mock<IBusinessLogic> logic = new Mock<IBusinessLogic>();
            //var list = Builder<string>.CreateListOfSize(30).Build();

            logic.Setup(m => m.GetList(21)).Returns(getDivisionList(21));
            FizzBuzzController controller   = new FizzBuzzController(logic.Object);
            var result                      = controller.FizzBuzzViewNext(new FizzBuzzModel { Page = new Pagging() { PageIndex = 2 }, Number = 21 }) as ViewResult;
            var viewModel                   = (FizzBuzzModel)result.Model;
            Assert.AreEqual(1, viewModel.DivisionList.Count); //Second page shouold have ony 1 Value
        }

        private List<IDivision> getDivisionList(int rows)
        {
            List<IDivision> list = new List<IDivision>();
            for (int index = 1; index <= rows; index++){
                list.Add(new SimpleDivision(index));
            }
            return list;
        }
    }
}

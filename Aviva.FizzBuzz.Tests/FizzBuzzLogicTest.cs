﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Aviva.FizzBuzz.Model;

namespace AvivaTest.Tests
{
    [TestFixture]
    public class FizzBuzzLogicTest
    {
        [Test]
        public void Input_number_24_should_get_list_with_24_numbers()
        {
            Aviva.FizzBuzz.Model.IBusinessLogic fizzBuzzLogic = new Aviva.FizzBuzz.Model.FizzBuzzLogic();
            List<IDivision> list = fizzBuzzLogic.GetList(24);
            Assert.AreEqual(24, list.Count);
            Assert.AreEqual("1", ((IDivision)list[0]).Message);
            Assert.AreEqual("2", ((IDivision)list[1]).Message);
        }

        [Test]
        public void Input_number_24_should_replace_3_divisible_with_proper_message_color()
        {
            Aviva.FizzBuzz.Model.IBusinessLogic fizzBuzzLogic = new Aviva.FizzBuzz.Model.FizzBuzzLogic();
            List<IDivision> list = fizzBuzzLogic.GetList(24);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz", ((IDivision)list[2]).Message);
                Assert.AreEqual("Wizz", ((IDivision)list[5]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz", ((IDivision)list[2]).Message);
                Assert.AreEqual("Fizz", ((IDivision)list[5]).Message);
            }
            Assert.AreEqual("blue", ((IDivision)list[2]).MessageColor);
            Assert.AreEqual("blue", ((IDivision)list[5]).MessageColor);
        }

        [Test]
        public void Input_number_24_should_replace_5_divisible_with_proper_message_color()
        {
            Aviva.FizzBuzz.Model.IBusinessLogic fizzBuzzLogic = new Aviva.FizzBuzz.Model.FizzBuzzLogic();
            List<IDivision> list = fizzBuzzLogic.GetList(24);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wuzz", ((IDivision)list[4]).Message);                
                Assert.AreEqual("Wuzz", ((IDivision)list[9]).Message);                                
            }
            else
            {
                Assert.AreEqual("Buzz", ((IDivision)list[4]).Message);
                Assert.AreEqual("Buzz", ((IDivision)list[9]).Message);
            }
            Assert.AreEqual("green", ((IDivision)list[4]).MessageColor);
            Assert.AreEqual("green", ((IDivision)list[9]).MessageColor);
        }

        [Test]
        public void Input_number_24_should_replace_3_and_5_divisible_with__proper_message()
        {
            Aviva.FizzBuzz.Model.IBusinessLogic fizzBuzzLogic = new Aviva.FizzBuzz.Model.FizzBuzzLogic();
            List<IDivision> list = fizzBuzzLogic.GetList(24);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz Wuzz", ((IDivision)list[14]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz Buzz", ((IDivision)list[14]).Message);
            }
        }
    }
}
